package com.chhaya.demomvp.ui.login.mvp;

public class LoginPresenter implements LoginMVP.Presenter {

    private LoginMVP.View view;
    private LoginMVP.Interactor interactor;

    public LoginPresenter() {
        this.interactor = new LoginInteractor();
    }

    @Override
    public void onLogin(String userName, String password) {
        view.onShowLoading();
        interactor.authenticate(userName, password, new LoginMVP.Interactor.OnAuthenticatedInteractor() {
            @Override
            public void onSuccess(String message) {
                view.onLoginSuccess(message);
                view.onHideLoading();
            }

            @Override
            public void onFail(String message) {
                view.onLoginFail(message);
                view.onHideLoading();
            }
        });
    }

    @Override
    public void setView(LoginMVP.View view) {
        this.view = view;
    }
}
