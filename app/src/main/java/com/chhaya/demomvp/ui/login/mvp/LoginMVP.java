package com.chhaya.demomvp.ui.login.mvp;

public interface LoginMVP {

    interface View {
        void onShowLoading();
        void onHideLoading();
        void onLoginSuccess(String message);
        void onLoginFail(String message);
    }

    interface Presenter {
        void onLogin(String userName, String password);
        void setView(View view);
    }

    interface Interactor {
        void authenticate(String userName,
                          String password,
                          OnAuthenticatedInteractor onAuthenticatedInteractor);

        interface OnAuthenticatedInteractor {
            void onSuccess(String message);
            void onFail(String message);
        }

    }

}
