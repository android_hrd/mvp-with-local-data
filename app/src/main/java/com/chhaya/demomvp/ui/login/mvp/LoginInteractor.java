package com.chhaya.demomvp.ui.login.mvp;

public class LoginInteractor implements LoginMVP.Interactor {

    final static String DEFAULT_USER_NAME = "system";
    final static String DEFAULT_PASSWORD = "12345";

    @Override
    public void authenticate(String userName, String password, OnAuthenticatedInteractor onAuthenticatedInteractor) {
        if (DEFAULT_USER_NAME.equals(userName)
                && DEFAULT_PASSWORD.equals(password)) {
            onAuthenticatedInteractor.onSuccess("Logged in successfully");
        } else {
            onAuthenticatedInteractor.onFail("Logged in failed successfully");
        }
    }

}
