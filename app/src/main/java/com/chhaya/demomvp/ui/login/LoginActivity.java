package com.chhaya.demomvp.ui.login;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.chhaya.demomvp.R;
import com.chhaya.demomvp.ui.login.mvp.LoginMVP;
import com.chhaya.demomvp.ui.login.mvp.LoginPresenter;

public class LoginActivity extends AppCompatActivity
implements LoginMVP.View {

    private EditText editUserName;
    private EditText editPassword;
    private ProgressBar pbLoading;
    private Button btnLogin;

    private LoginPresenter loginPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        onViewInitialized();

        loginPresenter = new LoginPresenter();
        loginPresenter.setView(this);

        pbLoading.setVisibility(View.GONE);

        btnLogin.setOnClickListener(v -> {
            String userName = editUserName.getText().toString();
            String password = editPassword.getText().toString();
            loginPresenter.onLogin(userName, password);
        });

    }

    private void onViewInitialized() {
        editUserName = findViewById(R.id.edit_username);
        editPassword = findViewById(R.id.edit_password);
        btnLogin = findViewById(R.id.button_login);
        pbLoading = findViewById(R.id.pb_loading);
    }

    @Override
    public void onShowLoading() {
        pbLoading.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHideLoading() {
        pbLoading.setVisibility(View.GONE);
    }

    @Override
    public void onLoginSuccess(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLoginFail(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
